﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsExample1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Saludos desde el main thread");
            
            Thread miHilo = new Thread(Greetings);
            miHilo.Start();

            Thread miHilo2 = new Thread(Mensaje);
            miHilo2.Start(3);


        }

        static void Greetings()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Saludos desde el hilo");
        }

        static void Mensaje(object o)
        {
            int mensaje = Convert.ToInt32(o);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Saludos desde el Mensaje {0}, {1}", mensaje, mensaje * 2);
        }

    }
}
