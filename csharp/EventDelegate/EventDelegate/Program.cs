﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            AddTwoNumbers a = new AddTwoNumbers();
            a.evOddNumber += new AddTwoNumbers.dgOddNumber(EventMessage);
            a.Add();
            Console.Read();
        }

        static void EventMessage()
        {
            Console.WriteLine("Event Executed: This is Odd number");
        }
    }

    class AddTwoNumbers
    {
        public delegate void dgOddNumber();
        public event dgOddNumber evOddNumber;

        public void Add()
        {
            for (int result = 0; result <= 10; result++)
            {
                Console.WriteLine(result.ToString());
                if ((result % 2 != 0) && (evOddNumber != null))
                {
                    evOddNumber();
                }
            }
        }
    }
}
