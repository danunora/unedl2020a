﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCode2
{
    class Program
    {
        static unsafe void Main(string[] args)
        {
            Unsafe1();
            Console.ReadKey();
        }

        public static unsafe void Unsafe1()
        {
            int var = 20;
            int* p = &var;
            Console.WriteLine("Normal Data is: {0} ", var);
            Console.WriteLine("Pointer Data is: {0} ", p->ToString());
            Console.WriteLine("Address is: {0} ", (int)p);
        }

    }
}
