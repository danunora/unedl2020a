﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCode
{
    class Program
    {
        unsafe static void Main(string[] args)
        {
            int ab = 32;
            int* p = &ab;
            Console.WriteLine("value of ab is {0}", *p);
            Console.ReadLine();
        }
    }
}
