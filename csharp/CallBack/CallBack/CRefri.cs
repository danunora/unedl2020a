﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBack
{
    class CRefri
    {
        private ArrayList listaSinks = new ArrayList();
        private int kilosAlimentos = 0;
        private int grados = 0;

        public CRefri(int kilosAlimentos, int grados)
        {
            this.kilosAlimentos = kilosAlimentos;
            this.grados = grados;
        }

        public void AgregarSink(IEventosRefri pSink)
        {
            if (pSink != null)
            {
                listaSinks.Add(pSink);
            }
        }

        public void EliminarSink(IEventosRefri pSink)
        {
            if (listaSinks.Contains(pSink))
            {
                listaSinks.Remove(pSink);
            }
        }

        public int Kilos { get { return kilosAlimentos; } }
        public int Grados { get { return grados;  } }

        public void Trabajar(int pConsumo)
        {
            kilosAlimentos -= pConsumo;
            grados += 1;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("{0} kilos, {1} grados", kilosAlimentos, grados);
            if (kilosAlimentos <= 10)
            {
                foreach(IEventosRefri handler in listaSinks)
                {
                    handler.EReservasBajas(kilosAlimentos);
                }
            }
            if(grados >= 0)
            {
                foreach(IEventosRefri handler in listaSinks)
                {
                    handler.EDescongelado(grados);
                }
            }
        }

    }
}
