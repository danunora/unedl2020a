﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBack
{
    class Program
    {
        static void Main(string[] args)
        {
            CRefri miRefri = new CRefri(50,-20);
            Random random = new Random();
            CRefriSink sink1 = new CRefriSink();
            CTiendaSink sink2 = new CTiendaSink();
            miRefri.AgregarSink(sink1);
            miRefri.AgregarSink(sink2);
            while(miRefri.Kilos > 0 && sink1.Paro == false)
            {
                miRefri.Trabajar(random.Next(1, 5));
            }
        }
    }
}
