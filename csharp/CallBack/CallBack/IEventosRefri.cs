﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBack
{
    interface IEventosRefri
    {
        void EReservasBajas(int pKilos);
        void EDescongelado(int pGrados);
    }
}
