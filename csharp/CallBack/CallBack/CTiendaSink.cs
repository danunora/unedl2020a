﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBack
{
    class CTiendaSink:IEventosRefri
    {
        public void EReservasBajas(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Le enviaremos sus viveres");
            Console.WriteLine("Seran {0} kilos", pKilos);
        }

        public void EDescongelado(int pGrados)
        {

        }
    }
}
