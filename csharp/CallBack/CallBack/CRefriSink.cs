﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBack
{
    class CRefriSink:IEventosRefri
    {
        private bool paro = false;
        public bool Paro { get { return paro; } }

        public void EReservasBajas(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Reservas Bajas de Alimentos");
            Console.WriteLine("Quedan {0} kilos", pKilos);
        }

        public void EDescongelado(int pGrados)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("El refri se descongela");
            if (pGrados > 0)
                paro = true;
        }
    }
}
