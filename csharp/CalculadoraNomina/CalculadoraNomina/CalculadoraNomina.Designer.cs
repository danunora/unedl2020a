﻿namespace CalculadoraNomina
{
    partial class CalculadoraNomina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDataCalc = new System.Windows.Forms.GroupBox();
            this.txtDiasNomina = new System.Windows.Forms.TextBox();
            this.txtSueldoDiario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpDetallePerc = new System.Windows.Forms.GroupBox();
            this.lblSueldoBruto = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.grpDataCalc.SuspendLayout();
            this.grpDetallePerc.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDataCalc
            // 
            this.grpDataCalc.Controls.Add(this.txtDiasNomina);
            this.grpDataCalc.Controls.Add(this.txtSueldoDiario);
            this.grpDataCalc.Controls.Add(this.label2);
            this.grpDataCalc.Controls.Add(this.label1);
            this.grpDataCalc.Location = new System.Drawing.Point(13, 13);
            this.grpDataCalc.Name = "grpDataCalc";
            this.grpDataCalc.Size = new System.Drawing.Size(473, 153);
            this.grpDataCalc.TabIndex = 0;
            this.grpDataCalc.TabStop = false;
            this.grpDataCalc.Text = "Datos para cálculo";
            // 
            // txtDiasNomina
            // 
            this.txtDiasNomina.Location = new System.Drawing.Point(138, 86);
            this.txtDiasNomina.Name = "txtDiasNomina";
            this.txtDiasNomina.Size = new System.Drawing.Size(298, 26);
            this.txtDiasNomina.TabIndex = 3;
            this.txtDiasNomina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiasNomina.TextChanged += new System.EventHandler(this.txtDiasNomina_TextChanged);
            // 
            // txtSueldoDiario
            // 
            this.txtSueldoDiario.Location = new System.Drawing.Point(138, 45);
            this.txtSueldoDiario.Name = "txtSueldoDiario";
            this.txtSueldoDiario.Size = new System.Drawing.Size(298, 26);
            this.txtSueldoDiario.TabIndex = 2;
            this.txtSueldoDiario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSueldoDiario.TextChanged += new System.EventHandler(this.txtSueldoDiario_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Días nómina: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sueldo diario: ";
            // 
            // grpDetallePerc
            // 
            this.grpDetallePerc.Controls.Add(this.lblSueldoBruto);
            this.grpDetallePerc.Controls.Add(this.label4);
            this.grpDetallePerc.Location = new System.Drawing.Point(13, 190);
            this.grpDetallePerc.Name = "grpDetallePerc";
            this.grpDetallePerc.Size = new System.Drawing.Size(473, 160);
            this.grpDetallePerc.TabIndex = 1;
            this.grpDetallePerc.TabStop = false;
            this.grpDetallePerc.Text = "Datos para cálculo";
            // 
            // lblSueldoBruto
            // 
            this.lblSueldoBruto.AutoSize = true;
            this.lblSueldoBruto.Location = new System.Drawing.Point(138, 44);
            this.lblSueldoBruto.Name = "lblSueldoBruto";
            this.lblSueldoBruto.Size = new System.Drawing.Size(113, 20);
            this.lblSueldoBruto.TabIndex = 1;
            this.lblSueldoBruto.Text = "lblSueldoBruto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Sueldo : ";
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(115, 371);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(119, 33);
            this.btnActualizar.TabIndex = 2;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(270, 371);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(119, 33);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reiniciar";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // CalculadoraNomina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 422);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.grpDetallePerc);
            this.Controls.Add(this.grpDataCalc);
            this.Name = "CalculadoraNomina";
            this.Text = "Calculadora de Nómina";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpDataCalc.ResumeLayout(false);
            this.grpDataCalc.PerformLayout();
            this.grpDetallePerc.ResumeLayout(false);
            this.grpDetallePerc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDataCalc;
        private System.Windows.Forms.TextBox txtDiasNomina;
        private System.Windows.Forms.TextBox txtSueldoDiario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpDetallePerc;
        private System.Windows.Forms.Label lblSueldoBruto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnReset;
    }
}

