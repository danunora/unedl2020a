﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : Form
    {
        public Calculadora()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

        }

        private void btnSub_Click(object sender, EventArgs e)
        {

        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "1";
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "2";
        }

        private void btnTres_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "3";
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "4";
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "5";
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "6";
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "7";
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "8";
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "9";
        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "0";
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Length != 0)
            {
                int index1 = txtResultado.Text.Length - 1;
                String temp1 = txtResultado.Text.Substring(index1);
                String temp2 = txtResultado.Text.Trim(
                    temp1.ToCharArray());
                txtResultado.Text = temp2;
            }
        }
    }
}
