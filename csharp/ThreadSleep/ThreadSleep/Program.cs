﻿using System;
using System.Threading;
class Example
{
    static void Main()
    {
        TimeSpan ts = new TimeSpan(0, 0, 10);

        Thread a = new Thread(Algo);
        a.Start(1);
        
        Thread b = new Thread(Algo);
        b.Start(2);

        Thread c = new Thread(Algo);
        c.Start(3);

        Thread d = new Thread(Algo);
        d.Start(4);

        Thread.Sleep(ts);

        Thread e = new Thread(Algo);
        e.Start(5);

        Thread aa = new Thread(Algo);
        aa.Start(6);

        Thread bb = new Thread(Algo);
        bb.Start(7);

        Thread cc = new Thread(Algo);
        cc.Start(8);

        Thread dd = new Thread(Algo);
        dd.Start(9);

        Thread ed = new Thread(Algo);
        ed.Start(10);

        Console.WriteLine("Main thread exits.");
    }

    static void Algo(object o)
    {
        Console.WriteLine("Thread: {0}", Convert.ToInt32(o));
    }

}

/* This example produces the following output:

Sleep for 2 seconds.
Sleep for 2 seconds.
Sleep for 2 seconds.
Sleep for 2 seconds.
Sleep for 2 seconds.
Main thread exits.
 */
