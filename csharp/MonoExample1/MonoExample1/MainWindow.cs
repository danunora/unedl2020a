using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{	
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		//Application.Quit ();
		//a.RetVal = true;
	}

	protected void OnBtnOkClicked (object sender, EventArgs e)
	{
		if (txtName.Text.Length != 0) {
			MessageDialog md1 = new MessageDialog (null, DialogFlags.Modal, 
			                                       MessageType.Info, 
			                                       ButtonsType.Ok, 
			                                       "Hello: " + txtName.Text);
			md1.Run (); 
			md1.Destroy ();
		} else {
			MessageDialog md2 = new MessageDialog (null, DialogFlags.Modal, 
			                                      MessageType.Info, 
			                                      ButtonsType.Ok, 
			                                      "Name cannot be empty!");
			md2.Run (); 
			md2.Destroy ();
		};
	}

	protected void OnBtnCancelClicked (object sender, EventArgs e)
	{
		Application.Quit ();
	}
}
