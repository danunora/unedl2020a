using System;
using Gtk;

public partial class Menu : Gtk.Window
{
	public Menu (): base(Gtk.WindowType.Toplevel)
	{
		Build ();
	}


	protected void OnMainWindowActionActivated (object sender, EventArgs e)
	{
		MainWindow mw = new MainWindow ();
		mw.Show ();
	}


	protected void OnVboxTestActionActivated (object sender, EventArgs e)
	{
		VboxTest vbt = new VboxTest ();
		vbt.Show ();
	}
}


