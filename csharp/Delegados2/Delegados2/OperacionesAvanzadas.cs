﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados2
{
    class OperacionesAvanzadas
    {

        public static int Mayor(int a, int b)
        {
            if (a == b)
                return a;
            if (a < b)
            {
                return b;
            } else
            {
                return a;
            }
        }
    }
}
