﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados2
{
    public delegate int MiDelegado(int a, int b);
    class Program
    {
        static void Main(string[] args)
        {
            MiDelegado delegado = new MiDelegado(OperacionesBasicas.Suma);

            Console.WriteLine("Suma 3 + 4: {0}", delegado(3,4).ToString());

            delegado = new MiDelegado(OperacionesAvanzadas.Mayor);

            Console.WriteLine("Mayor 3 y 4: {0}", delegado(3,4).ToString());


        }
    }
}
