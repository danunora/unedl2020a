﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCode3
{
    class Program
    {
        static unsafe void Main(string[] args)
        {
            Program p = new Program();
            int var1 = 10;
            int var2 = 20;
            int* x = &var1;
            int* y = &var2;
            Console.WriteLine("Before Swap: var1:{0}, var1address: {2},  " +
                "var2: {1} var2address: {3}", var1, var2, (int)x, (int)y);
            p.swap(x, y);
            Console.WriteLine("After Swap: var1:{0}, var2: {1}", var1, var2);
            p.lista();
            Console.ReadKey();
        }

        public unsafe void swap(int* p, int* q)
        {
            int temp = *p;
            *p = *q;
            *q = temp;
        }

        public unsafe void lista()
            {
                int[] list = { 10, 20, 30, 40, 50 };
                fixed (int* ptr = list)
                    for (int i = 0; i < list.Length; i++)
                    {
                        Console.WriteLine("Address of list[{0}]={1}", i, (int)(ptr + i));
                        Console.WriteLine("Value of list[{0}]={1}", i, *(ptr + i));
                    }
                Console.ReadKey();
            }
    }
}
