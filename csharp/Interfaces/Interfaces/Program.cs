﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            double valor1 = 0.0;
            double valor2 = 0.0;

            IOperaciones operaciones = new CSuma();

            while(opcion != 5)
            {
                Console.WriteLine("1) Suma, 2) Resta, 3) Multiplicacion, 4) Division 5) Salir");
                Console.WriteLine("Seleccione Opcion");
                opcion = Convert.ToInt32(Console.ReadLine());

                switch (opcion) {
                    case 1 :
                        operaciones = new CSuma();
                        break;
                    case 2:
                        operaciones = new CResta();
                        break;
                    case 3:
                        operaciones = new CMultiplica();
                        break;
                    case 4:
                        operaciones = new CDivide();
                        break;
                }
                if (opcion != 5)
                {
                    Console.WriteLine("Introduce primer valor: ");
                    valor1 = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Introduce primer valor: ");
                    valor2 = Convert.ToInt32(Console.ReadLine());

                    operaciones.calcular(valor1, valor2);
                    operaciones.mostrar();
                }
            }
        }
    }
}
