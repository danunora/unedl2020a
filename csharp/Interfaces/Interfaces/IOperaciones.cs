﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    interface IOperaciones
    {
        void calcular(double a, double b);
        void mostrar();
    }
}
