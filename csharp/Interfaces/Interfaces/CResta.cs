﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class CResta : IOperaciones
    {
        private double resultado = 0;
        private ArrayList resultados = new ArrayList();

        public void calcular(double a, double b)
        {
            resultado = a - b;
        }

        public void mostrar()
        {
            Console.WriteLine("Resultado Resta: {0}", resultado);
            resultados.Add(resultado);
        }

        public void muestraResultados()
        {
            foreach (double resultado in resultados)
                Console.WriteLine(resultado);
        }

    }
}

