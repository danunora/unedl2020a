﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
    public delegate int MiDelegado(int a, int b); 
    class Program
    {
        static void Main(string[] args) {

            MiDelegado delegado = new MiDelegado(OperacionesBasicas.Suma);

            Console.WriteLine("Suma: {0}", delegado(5,6).ToString());

            delegado = new MiDelegado(OperacionesBasicas.Resta);

            Console.WriteLine("Resta: {0}", delegado(5, 6).ToString());

            delegado = new MiDelegado(OperacionesAvanzadas.Mayor);

            Console.WriteLine("Mayor: {0}", delegado(5, 6).ToString());

        }
    }
}
