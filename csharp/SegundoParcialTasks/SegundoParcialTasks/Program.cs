﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SegundoParcialTasks
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        public static void ConstruyePared(object o)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(o);
            }
        }

        public static void habitacion(object o)
        {
            Console.WriteLine("Iniciando la habitación: {0}",o);
            pooldechalanes.WaitOne();
            var paredes = new Task[4];
            for (int i = 0; i <= paredes.Length - 1; i++) {
                paredes[i] = Task.Run(() => ConstruyePared('.'));
            }
            try {
                Task.WaitAll(paredes);
                Console.WriteLine("------------------");
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");
                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine("   {0}", ex.Message);
            }
            foreach (var p in paredes)
                Console.WriteLine(" pared: {0}: {1}", p.Id, p.Status);
            Console.WriteLine("habitación {0} terminada, turno {1}",o,
                pooldechalanes.Release());
        }

        public static void Main(string[] args)
        {
            //Task.Factory.StartNew(() => ConstruyePared('A'));

            //var t = new Task(() => ConstruyePared('B'));
            //t.Start();

            //ConstruyePared('-');

            Console.WriteLine("¿Cuántas habitaciones?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos chalanes pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            pooldechalanes = new Semaphore(0, m);

            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= n; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(habitacion));
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);

            Thread.Sleep(o);

            Console.WriteLine("Construcción terminada");
            Console.ReadKey();
        }
    }
}
