﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicio Main Method");

            Task mytask1 = new Task(Nomina);
            mytask1.Start();

            Task task2 = Task.Factory.StartNew(Pagos);

            Task task3 = Task.Run(() => { Impresiones(); });

            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando main {0}", i);
            }

            Console.WriteLine("Fin Main Method");
            Console.ReadKey();
        }

        static void Nomina()
        {
            Console.WriteLine("Inicio Nomina");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando nomina {0}",i);
            }
            Console.WriteLine("Fin Nomina");
        }

        static void Pagos()
        {
            Console.WriteLine("Inicio Pagos");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando pagos {0}", i);
            }
            Console.WriteLine("Fin Pagos");
        }

        static void Impresiones()
        {
            Console.WriteLine("Inicio impresiones");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando impresiones {0}", i);
            }
            Console.WriteLine("Fin Impresiones");
        }

    }
}
