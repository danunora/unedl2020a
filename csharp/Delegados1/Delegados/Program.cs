﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
    public delegate void MiDelegado(String mensaje);
    class Program
    {
        static void Main(string[] args)
        {
            MiDelegado delegado = new MiDelegado(ViaRadio.Mensaje);

            delegado("Hola a todos !!");

            delegado = new MiDelegado(ViaTelevision.Mensaje);

            delegado("nuevo mensaje");

        }
    }
}
