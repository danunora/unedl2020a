﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
    class ViaRadio
    {
        public static void Mensaje(String mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Mensaje Via Radio: {0}", mensaje);
        }
    }
}
