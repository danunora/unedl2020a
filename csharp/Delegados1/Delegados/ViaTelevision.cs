﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
    class ViaTelevision
    {
        public static void Mensaje(String mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Mensaje Via Television: {0}", mensaje);
        }

    }
}
