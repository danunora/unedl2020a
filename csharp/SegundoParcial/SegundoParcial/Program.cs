﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SegundoParcial
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;

        private static void ChalanAlbanil(object chalan)
        {
            Console.WriteLine("Chalan de Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Chalan {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Chalan {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Chalan {0} termina turno...", chalan);
            Console.WriteLine("Chalan {0} libera su lugar {1}",
                chalan, pooldechalanes.Release());
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos chalanes llegaron a chambear?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            pooldechalanes = new Semaphore(0, m);

            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= n; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);

            Thread.Sleep(o);
            Console.WriteLine("Se terminó la construcción");

        }
    }
}
